import psycopg2

from config import user, password, dbname, host, records_data

conn = psycopg2.connect(dbname=dbname, user=user, password=password, host=host)


def create_tables():
    """
    Создает в бд таблицы location, department и employee
    :return:
    """
    with conn.cursor() as cursor:
        cursor.execute('''CREATE TABLE IF NOT EXISTS location (
                            id integer PRIMARY KEY, 
                            name VARCHAR (255) NOT NULL)''')
        cursor.execute('''CREATE TABLE IF NOT EXISTS department (id integer PRIMARY KEY, 
                            name VARCHAR (255) NOT NULL, 
                            location_id integer)''')
        cursor.execute('''CREATE TABLE IF NOT EXISTS employee (id integer PRIMARY KEY, 
                            name VARCHAR (255) NOT NULL, 
                            department_id integer)''')
        conn.commit()


def add_records(data):
    """
    Наполняет таблицы данными, записаными в data
    :param data:
    :return:
    """
    with conn.cursor() as cursor:
        for record in data:
            if record['Type'] == 1:
                cursor.execute(f'INSERT INTO location(id, name) VALUES ({record["id"]}, %s)', (record['Name'],))
            if record['Type'] == 2:
                cursor.execute(
                    f'INSERT INTO department(id, name, location_id) VALUES ({record["id"]}, %s, {record["ParentId"]})',
                    (record['Name'],))
            if record['Type'] == 3:
                cursor.execute(
                    f'INSERT INTO employee(id, name, department_id) VALUES ({record["id"]}, %s, {record["ParentId"]})',
                    (record['Name'],))
        conn.commit()


def get_by_id(id: int):
    """
    По id сотрутника вызвращает имена всех сотрудников, работающих в том же офисе
    :param id:
    :return:
    """
    with conn.cursor() as cursor:
        cursor.execute(f'''
                        SELECT e_2.name 
                            FROM employee as e_1 JOIN department as d_1 
                                ON (e_1.department_id = d_1.id) AND e_1.id = {id}
                            JOIN location as l
                                ON d_1.location_id = l.id
                            JOIN department as d_2
                                ON l.id = d_2.location_id
                            JOIN employee as e_2
                                ON d_2.id = e_2.department_id
                        ''')
        return list(map(lambda x: x[0], cursor))


def initiation():
    create_tables()
    add_records(records_data)


if __name__ == '__main__':
    initiation()
