from helpers import get_by_id

if __name__ == '__main__':
    try:
        while True:
            emp_id = int(input('Введите id сотрудника: '))
            names = get_by_id(emp_id)
            if names:
                print(*names, sep=', ')
            else:
                print('Сотрудников не найдено')
    except KeyboardInterrupt:
        print('Завершение программы')
